package com.example.yunjeong.infiniteviewpager.ui;

import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.yunjeong.infiniteviewpager.R;
import com.example.yunjeong.infiniteviewpager.adapter.FragmentContentsPagerAdapter;
import com.example.yunjeong.infiniteviewpager.databinding.ActivityMainBinding;
import com.example.yunjeong.infiniteviewpager.ui.widget.AutoScrollPager;


public class MainActivity extends FragmentActivity {

    private AutoScrollPager mPager;
    private PagerAdapter mAdapter;
    private int pageState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mAdapter = new FragmentContentsPagerAdapter(this, getSupportFragmentManager());

        mainBinding.pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("pageScroll", position+"");
                if(pageState == ViewPager.SCROLL_STATE_IDLE) {

                }

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("pageSelected", position+"");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                pageState = state;

            }
        });

        mainBinding.pager.setAdapter(mAdapter);
        Log.d("virtualPosi", (Integer.MAX_VALUE / 2) % 5 +""); // 반으로 나누었을떄 현재 VirtualPosi
        mainBinding.pager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % 5);
        mainBinding.pager.StartAutoScroll();
    }

//    @Override
//    public void onBackPressed() {
//        if(mPager.getCurrentItem() == 0) {
//            super.onBackPressed();
//        } else {
//            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
//        }
//    }
}

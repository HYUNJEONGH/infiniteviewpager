package com.example.yunjeong.infiniteviewpager.util;

import android.util.Log;

import com.example.yunjeong.infiniteviewpager.BuildConfig;

/**
 * Created by YunSin on 2017-02-13.
 */

public class LogUtils {
    private static final String LOG_PREFIX = "infinite_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    public static boolean LOGGING_ENABLED = !BuildConfig.BUILD_TYPE.equalsIgnoreCase("release");

    public static String makeLogTag(String str) {
        if(str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return  LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH -1);
        }

        return  LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class cls) { return  makeLogTag(cls.getSimpleName()); }

    public static void LOGD(final String tag, String message) {
        if(LOGGING_ENABLED) {
            if (Log.isLoggable(tag, Log.DEBUG)) {
                Log.d(tag, message);
            }
        }
    }

}

package com.example.yunjeong.infiniteviewpager.ui.widget;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Created by YunSin on 2017-02-14.
 */

public class CustomScroller extends Scroller {

    public CustomScroller(Context context) {
        super(context);
    }

    public CustomScroller(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }
}

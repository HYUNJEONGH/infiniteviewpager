package com.example.yunjeong.infiniteviewpager.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yunjeong.infiniteviewpager.R;
import com.example.yunjeong.infiniteviewpager.databinding.FragmentContentsSlidePageBinding;

public class ContentsSlidePageFragment extends Fragment {

    public String[] text = {"일","이","삼","사","오"};
    FragmentContentsSlidePageBinding binding;

    public ContentsSlidePageFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_contents_slide_page, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding = FragmentContentsSlidePageBinding.bind(getView());
        binding.setText(text[getArguments().getInt("position")]);
    }
}

package com.example.yunjeong.infiniteviewpager.ui.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.animation.Interpolator;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

import static com.example.yunjeong.infiniteviewpager.util.LogUtils.LOGD;
import static com.example.yunjeong.infiniteviewpager.util.LogUtils.makeLogTag;

/**
 * Created by YunSin on 2017-02-14.
 */

public class AutoScrollPager extends ViewPager {

    private final static String TAG = makeLogTag(AutoScrollPager.class);

    private CustomScroller scroller = null;
    public static final int        DEFAULT_INTERVAL            = 4000;
    public static final int        LEFT                          = 0;
    public static final int        RIGHT                         = 1;

    /** whether automatic cycle when auto scroll reaching the last or first item, default is true **/
    private boolean isCycle = true;

    private boolean isBorderAnimation = true;

    private boolean isAutoScroll = false;

    /** scroll factor for auto scroll animation, default is 1.0 **/
    private double autoScrollFactor = 1.0;
    /** scroll factor for swipe scroll animation, default is 1.0 **/
    private double swipeScrollFactor = 1.0;

    private long interval = DEFAULT_INTERVAL;

    private int direction = RIGHT;

    public static final int SCROLL_WHAT = 0;

    private MyHandler handler;

    public AutoScrollPager(Context context) {
        super(context);
        initPager();
    }

    public AutoScrollPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initPager();
    }
    /**
     * set ViewPager scroller to change animation duration when sliding
     */
    void initPager() {
        handler = new MyHandler(this);
        setVIewPagerScroller();
    }

    //뷰페이저에 해당 Field를 만들어서 접근 가능하게한다.
    private void setVIewPagerScroller() {
        try {
            Field scrollerField = ViewPager.class.getDeclaredField("mScroller");
            scrollerField.setAccessible(true);
            Field interpolatorField = ViewPager.class.getDeclaredField("sInterpolator");
            interpolatorField.setAccessible(true);
            scroller = new CustomScroller(getContext(), (Interpolator)interpolatorField.get(null));
            scrollerField.set(this, scroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void StartAutoScroll() {
        isAutoScroll = true;
        sendScrollMessages((long)(interval + scroller.getDuration() / autoScrollFactor * swipeScrollFactor));
    }

    private void sendScrollMessages(long delayTimeInMills) {
        handler.removeMessages(SCROLL_WHAT);
        handler.sendEmptyMessageDelayed(SCROLL_WHAT, delayTimeInMills);
    }

    public void scrollOnce() {
        PagerAdapter adapter = getAdapter();
        int currentItem = getCurrentItem();
        int totalCount;

        //adapter is null or totalcount <= 1, no need scroll
        if(adapter == null || (totalCount = adapter.getCount()) <= 1) {
            return;
        }

        int nextItem = (direction == LEFT) ? --currentItem : ++currentItem;
        LOGD(TAG, "nextItem total:"+totalCount+","+ nextItem);
        if(nextItem < 0) {
                if(isCycle) {
                    setCurrentItem(totalCount - 1, isBorderAnimation);
                }
        } else if (nextItem == totalCount) {
            if (isCycle) {
                setCurrentItem(0, isBorderAnimation);
            }
        } else {
            setCurrentItem(nextItem, true);
        }

    }

    private static class MyHandler extends Handler {
        private final WeakReference<AutoScrollPager> autoScrollPager;

        private MyHandler(AutoScrollPager autoScrollPager) {
            this.autoScrollPager = new WeakReference<AutoScrollPager>(autoScrollPager);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SCROLL_WHAT:
                    AutoScrollPager pager = this.autoScrollPager.get();
                    if (pager != null) {
                        pager.scrollOnce();
                        pager.sendScrollMessages(pager.interval + pager.scroller.getDuration());
                    }
                default:
                    break;
            }
        }

    }

}




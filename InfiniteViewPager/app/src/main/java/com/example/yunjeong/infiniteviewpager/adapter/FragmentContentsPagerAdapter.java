package com.example.yunjeong.infiniteviewpager.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.example.yunjeong.infiniteviewpager.ui.ContentsSlidePageFragment;
import com.example.yunjeong.infiniteviewpager.ui.widget.AutoScrollPager;

import static com.example.yunjeong.infiniteviewpager.util.LogUtils.LOGD;
import static com.example.yunjeong.infiniteviewpager.util.LogUtils.makeLogTag;

/**
 * Created by YunSin on 2017-02-13.
 */

public class FragmentContentsPagerAdapter extends FragmentPagerAdapter {

    private final static String TAG = makeLogTag(FragmentContentsPagerAdapter.class);
    private static final int NUM_PAGES = 5;

    private Context mContext;

    private  ContentsSlidePageFragment[] mFragments;

    private FragmentManager mFragmentManager;

    public FragmentContentsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        mFragmentManager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        LOGD(TAG, "Creating fragment #" + position);

        // Reuse cached fragment if present
        if (mFragments != null && mFragments.length > position && mFragments[position] != null) {
            return mFragments[position];
        }

        //position
        ContentsSlidePageFragment frag = new ContentsSlidePageFragment();

        Bundle args = new Bundle();

        args.putInt("position", position);

        frag.setArguments(args);

        if(mFragments == null) {
            mFragments = new ContentsSlidePageFragment[NUM_PAGES];
        }

        mFragments[position] = frag;

        return frag;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int virtualPosi = position % NUM_PAGES;
        return super.instantiateItem(container, virtualPosi);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        int virtualPosi = position % NUM_PAGES;
        super.destroyItem(container, virtualPosi, object);
    }

    /**
     * @return all the cached {@link ContentsSlidePageFragment}s used by this Adapter.
     */
    public ContentsSlidePageFragment[] getFragments() {
        if (mFragments == null) {
            // Force creating the fragments
            int count = getCount();
            for (int i = 0; i < count; i++) {
                getItem(i);
            }
        }
        return mFragments;
    }

    public ContentsSlidePageFragment getFragment(int position) {
        if (mFragments == null) {
            // Force creating the fragments
            return (ContentsSlidePageFragment)getItem(position);
        }
        return mFragments[position];
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

}
